import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
	id("org.springframework.boot") version "2.2.2.RELEASE"
	id("io.spring.dependency-management") version "1.0.8.RELEASE"
	kotlin("jvm") version "1.3.61"
	kotlin("plugin.spring") version "1.3.61"
}

group = "com.eordie.inventory"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_1_8

repositories {
	mavenCentral()
	mavenLocal()
	maven { setUrl("http://repo.spring.io/libs-milestone") }
	maven { setUrl("http://repo.spring.io/libs-snapshot") }
	maven { setUrl("http://oss.jfrog.org/artifactory/oss-snapshot-local/") }
}

dependencies {
	implementation("org.springframework.boot:spring-boot-starter-actuator")
	implementation("org.springframework.boot:spring-boot-starter-security")
	implementation("org.springframework.boot:spring-boot-starter-webflux")

	implementation("org.zalando:problem-spring-webflux:0.25.2")
	implementation("io.springfox:springfox-swagger2:3.0.0-SNAPSHOT")
	implementation("io.springfox:springfox-spring-webflux:3.0.0-SNAPSHOT")
	implementation("org.springframework.data:spring-data-r2dbc:1.0.0.RC1")
	implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor:1.3.2")

	implementation("io.jsonwebtoken:jjwt-impl:0.10.7")
	implementation("io.jsonwebtoken:jjwt-jackson:0.10.7")
	implementation("com.coreoz:windmill:1.2.2-SNAPSHOT")
	implementation("com.devskiller.friendly-id:friendly-id-jackson-datatype:1.1.0")
	implementation("javax.validation:validation-api:2.0.1.Final")
	implementation("io.r2dbc:r2dbc-postgresql:0.8.0.RELEASE")
    implementation("org.flywaydb:flyway-core:5.2.4")
    implementation("org.postgresql:postgresql:42.2.6")

	// missing netty deps?
	implementation("io.netty:netty-codec-http:4.1.42.Final")
	implementation("io.netty:netty-codec-http2:4.1.42.Final")
	implementation("io.netty:netty-handler-proxy:4.1.42.Final")

	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    testImplementation("org.testcontainers:postgresql:1.11.4")
	testImplementation("org.springframework.boot:spring-boot-starter-test")
	testImplementation("org.springframework.security:spring-security-test")
	testImplementation("io.projectreactor:reactor-test")
	testImplementation("io.projectreactor:reactor-tools")
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict", "-Xjvm-default=enable")
		jvmTarget = "1.8"
	}
}
