package com.eordie.inventory.products.config

import com.eordie.inventory.products.utils.User
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Test

class TokenFactoryTest {

    private val user = User(1, 1,"admin", "admin", listOf("sku:read", "sku:write"))

    @Test
    fun should_create_reusable_token_for_user() {
        val token = TokenFactory.createToken(user)
        assertNotNull(token)

        val actual = TokenFactory.parseFromToken(token)
        assertNotNull(actual)

        assertEquals(user.roles, actual!!.roles)
        assertEquals(user.username, actual.username)
        assertEquals(user.userId, actual.userId)
        assertEquals(user.shardId, actual.shardId)
    }
}