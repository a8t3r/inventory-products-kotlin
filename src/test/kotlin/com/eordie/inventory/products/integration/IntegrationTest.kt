package com.eordie.inventory.products.integration

import com.eordie.inventory.products.Application
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ContextConfiguration

@kotlin.annotation.Target(AnnotationTarget.TYPE, AnnotationTarget.CLASS)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@SpringBootTest(classes = [Application::class], webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = [TestConfiguration::class], loader = CustomContextLoaderProvider.CustomContextLoader::class)
annotation class IntegrationTest