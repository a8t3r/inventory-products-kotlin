package com.eordie.inventory.products.integration

import org.springframework.boot.test.context.SpringBootContextLoader
import org.springframework.test.context.MergedContextConfiguration
import org.testcontainers.containers.PostgreSQLContainer
import reactor.core.publisher.Hooks
import reactor.tools.agent.ReactorDebugAgent
import java.net.URI

class KPostgresContainer(imageName: String) : PostgreSQLContainer<KPostgresContainer>(imageName)

object CustomContextLoaderProvider {

    private val postgres = KPostgresContainer("postgres:9.5.9")

    init {
        ReactorDebugAgent.init()
        Hooks.onOperatorDebug()

        postgres.start()
        Runtime.getRuntime().addShutdownHook(Thread { postgres.stop() })
    }

    class CustomContextLoader : SpringBootContextLoader() {
        override fun getInlinedProperties(config: MergedContextConfiguration): Array<String> {
            val url = URI.create(postgres.jdbcUrl.substringAfter(':'))
            return super.getInlinedProperties(config) + arrayOf(
                    "spring.multitenant.sources.1.host=" + url.host,
                    "spring.multitenant.sources.1.port=" + url.port,
                    "spring.multitenant.sources.1.database=" + url.path.substring(1),
                    "spring.multitenant.sources.1.username=" + postgres.username,
                    "spring.multitenant.sources.1.password=" + postgres.password,
                    "spring.jpa.hibernate.ddl-auto=none",
                    "spring.jpa.properties.hibernate.hbm2ddl.auto=none",
                    "spring.cloud.zookeeper.enabled=false")
        }
    }
}
