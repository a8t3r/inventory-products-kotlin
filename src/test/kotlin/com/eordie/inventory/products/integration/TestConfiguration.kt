package com.eordie.inventory.products.integration

import com.eordie.inventory.products.config.SecurityConfig
import com.eordie.inventory.products.utils.User
import org.springframework.boot.autoconfigure.AutoConfigureBefore
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.security.core.userdetails.ReactiveUserDetailsService
import org.springframework.security.core.userdetails.UserDetails
import reactor.core.publisher.Mono

@Configuration
@AutoConfigureBefore(SecurityConfig::class)
class TestConfiguration {

    @Bean
    @Primary
    fun reactiveUserDetailsOverride(): ReactiveUserDetailsService {
        return SimpleUserDetailsService(
                User(100500, 1,"nobodyelse", "****", listOf("sku:read", "sku:write")),
                User(100501, 1,"nobodyelse:readonly", "****", listOf("sku:read")),
                User(100502, 1,"nopermissions", "****", listOf()),
                User(100503, 1,"foobar", "****", listOf("foo:bar"))
        )
    }

    @Bean
    @Primary
    fun shardFunctionOverride(): Mono<Any> {
        return Mono.just(1.toShort())
    }
}

internal class SimpleUserDetailsService(private vararg val users: UserDetails): ReactiveUserDetailsService {
    override fun findByUsername(username: String?): Mono<UserDetails> {
        return users.filter { it.username == username }
                .map { Mono.just(it) }
                .first()
    }
}