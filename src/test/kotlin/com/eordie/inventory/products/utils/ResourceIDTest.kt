package com.eordie.inventory.products.utils

import org.junit.Assert.assertEquals
import org.junit.Test

class ResourceIDTest {

    @Test
    fun should_parse_identifier() {
        val id = ResourceID.parse(241294492511762325)

        assertEquals(3429.toShort(), id.shardId)
        assertEquals(1.toByte(), id.typeId)
        assertEquals(7075733, id.localId)
    }

    @Test
    fun should_parse_string_identifier() {
        val id = ResourceID.parse("Hp88qxkBNN")

        assertEquals(3429.toShort(), id.shardId)
        assertEquals(1.toByte(), id.typeId)
        assertEquals(7075733, id.localId)
    }

    @Test
    fun should_be_present_as_number() {
        val id = ResourceID(3429, 1, 7075733)

        assertEquals(241294492511762325, id.toNumber())
    }

    @Test
    fun should_be_present_as_string() {
        val id = ResourceID(3429, 1, 7075733)

        assertEquals("Hp88qxkBNN", id.toString())
    }
}