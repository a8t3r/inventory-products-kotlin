package com.eordie.inventory.products.api

import com.eordie.inventory.products.model.product.Tare
import java.math.BigDecimal

class UpdatedFields {

    var name: String? = null
    var brand: String? = null
    var price: BigDecimal? = null
    var quantity: BigDecimal? = null
    var tare: Tare? = null
    var tareCapacity: BigDecimal? = null

}