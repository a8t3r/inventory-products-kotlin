package com.eordie.inventory.products.api

import com.eordie.inventory.products.integration.IntegrationTest
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithUserDetails
import org.springframework.test.context.junit4.SpringRunner
import org.testcontainers.shaded.com.google.common.io.Resources

@IntegrationTest
@RunWith(SpringRunner::class)
class SkuControllerSecurityTest : AbstractIntegrationTest() {

    @Before
    fun setUp() {
        truncate()
    }

    @Test
    @WithUserDetails("nobodyelse")
    fun should_allow_read_request_for_admin_user() {
        client.get().uri("/skus")
                .exchange()
                .expectStatus()
                .isOk
    }

    @Test
    @WithUserDetails("nobodyelse")
    fun should_allow_write_request_for_admin_user() {
        client.post().uri("/skus")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(Resources.toByteArray(Resources.getResource("item.json")))
                .exchange()
                .expectStatus()
                .isCreated
    }

    @Test
    @WithUserDetails("nobodyelse:readonly")
    fun should_allow_read_request_for_readonly_user() {
        client.get().uri("/skus")
                .exchange()
                .expectStatus()
                .isOk
    }

    @Test
    @WithUserDetails("nobodyelse:readonly")
    fun should_not_allow_write_request_for_readonly_user() {
        client.post().uri("/skus")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(Resources.toByteArray(Resources.getResource("item.json")))
                .exchange()
                .expectStatus()
                .isForbidden
    }

    @Test
    @WithUserDetails("foobar")
    fun should_not_allow_read_request_for_unauthorized() {
        client.get().uri("/skus")
                .exchange()
                .expectStatus()
                .isForbidden
    }

    @Test
    @WithUserDetails("foobar")
    fun should_not_allow_write_request_for_unauthorized() {
        client.post().uri("/skus")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(Resources.toByteArray(Resources.getResource("item.json")))
                .exchange()
                .expectStatus()
                .isForbidden
    }

    @Test
    @WithUserDetails("nopermissions")
    fun should_not_allow_read_request_without_authority() {
        client.get().uri("/skus")
                .exchange()
                .expectStatus()
                .isForbidden
    }

    @Test
    @WithUserDetails("nopermissions")
    fun should_not_allow_write_request_without_authority() {
        client.post().uri("/skus")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(Resources.toByteArray(Resources.getResource("item.json")))
                .exchange()
                .expectStatus()
                .isForbidden
    }
}