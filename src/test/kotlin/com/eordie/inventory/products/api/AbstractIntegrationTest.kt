package com.eordie.inventory.products.api

import com.eordie.inventory.products.service.SkuRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.web.reactive.server.WebTestClient

abstract class AbstractIntegrationTest {

    @Autowired
    protected lateinit var client: WebTestClient

    @Autowired
    private lateinit var repository: SkuRepository

    fun truncate() {
        repository.deleteAll().block()
    }
}