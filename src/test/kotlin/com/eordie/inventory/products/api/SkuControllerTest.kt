package com.eordie.inventory.products.api

import com.eordie.inventory.products.integration.IntegrationTest
import com.eordie.inventory.products.model.product.SkuView
import com.eordie.inventory.products.model.product.Tare
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithUserDetails
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.reactive.server.WebTestClient
import org.testcontainers.shaded.com.google.common.io.Resources
import java.io.IOException
import java.math.BigDecimal
import com.eordie.inventory.products.config.WebConfig.Companion.defaultMapper as mapper

@IntegrationTest
@RunWith(SpringRunner::class)
@WithUserDetails("nobodyelse")
class SkuControllerTest : AbstractIntegrationTest() {

    @Before
    fun setUp() {
        truncate()
    }

    @Test
    fun should_process_single_item_json() {
        val id = createResource("item.json")
                .expectBody(object : ParameterizedTypeReference<SkuView>() {})
                .returnResult().responseBody!!.id.toString()

        client.get().uri("/skus/{id}", id)
                .exchange()
                .expectStatus()
                .isOk
                .expectBody()
                .jsonPath("length()").isEqualTo(10)

        client.delete().uri("/skus/{id}", id)
                .exchange()
                .expectStatus()
                .isNoContent
    }

    @Test
    fun should_find_by_name_filter() {
        createResources("items_with_same_name.json")
                .expectBody()
                .jsonPath("length()").isEqualTo(5)

        client.get().uri("/skus?name={name}", "first")
                .exchange()
                .expectStatus()
                .isOk
                .expectBody()
                .jsonPath("length()").isEqualTo(2)
                .jsonPath("items.length()").isEqualTo(4)
                .jsonPath("paging.length()").isEqualTo(0)
    }

    @Test
    fun should_find_by_brand_filter() {
        createResources("items_with_same_name.json")
                .expectBody()
                .jsonPath("length()").isEqualTo(5)

        client.get().uri("/skus?brand={name}", "gabbana")
                .exchange()
                .expectStatus()
                .isOk
                .expectBody()
                .jsonPath("length()").isEqualTo(2)
    }

    @Test
    fun should_find_by_name_brand_filter() {
        createResources("items_with_same_name.json")
                .expectBody()
                .jsonPath("length()").isEqualTo(5)

        client.get().uri("/skus?name={name}&brand={brand}", "first", "dolce")
                .exchange()
                .expectStatus()
                .isOk
                .expectBody()
                .jsonPath("length()").isEqualTo(2)
                .jsonPath("items.length()").isEqualTo(3)
                .jsonPath("paging.length()").isEqualTo(0)
    }

    @Test
    fun should_find_by_quantity_filter() {
        createResources("items_with_same_name.json")
                .expectBody()
                .jsonPath("length()").isEqualTo(5)

        client.get().uri("/skus?quantity<={quantity}", 4)
                .exchange()
                .expectStatus()
                .isOk
                .expectBody()
                .jsonPath("length()").isEqualTo(2)
    }

    @Test
    fun should_support_create_and_update_lifecycle() {
        var updatedFields = UpdatedFields()
        updatedFields.name = "foo"
        updatedFields.brand = "???"
        updatedFields.price = BigDecimal("10")
        updatedFields.quantity = BigDecimal.ZERO
        updatedFields.tare = Tare.liter
        updatedFields.tareCapacity = BigDecimal("0.5")

        val id = client.post().uri("/skus/")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(mapper.writeValueAsBytes(updatedFields))
                .exchange()
                .expectStatus()
                .isCreated
                .expectBody(object : ParameterizedTypeReference<SkuView>() {})
                .returnResult().responseBody!!.id.toString()

        updatedFields = UpdatedFields()
        updatedFields.brand = "bar"
        updatedFields.price = BigDecimal("42")
        updatedFields.quantity = BigDecimal("100500")

        client.patch().uri("/skus/{id}", id)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(mapper.writeValueAsBytes(updatedFields))
                .exchange()
                .expectStatus()
                .isOk
                .expectBody()
                .jsonPath("length()").isEqualTo(10)
                .jsonPath("id").isEqualTo(id)
                .jsonPath("user_id").isEqualTo(100500)
                .jsonPath("name").isEqualTo("foo")
                .jsonPath("brand").isEqualTo("bar")
                .jsonPath("price").isEqualTo(42)
                .jsonPath("quantity").isEqualTo(100500)
                .jsonPath("tare").isEqualTo("liter")
                .jsonPath("tare_capacity").isEqualTo(0.5)
                .jsonPath("created_at").exists()
                .jsonPath("updated_at").exists()

        client.get().uri("/skus/{id}", id)
                .exchange()
                .expectStatus()
                .isOk
                .expectBody()
                .jsonPath("length()").isEqualTo(10)
                .jsonPath("id").isEqualTo(id)
                .jsonPath("user_id").isEqualTo(100500)
                .jsonPath("name").isEqualTo("foo")
                .jsonPath("brand").isEqualTo("bar")
                .jsonPath("price").isEqualTo(42)
                .jsonPath("quantity").isEqualTo(100500)
                .jsonPath("tare").isEqualTo("liter")
                .jsonPath("tare_capacity").isEqualTo(0.5)
                .jsonPath("created_at").exists()
                .jsonPath("updated_at").exists()
    }

    @Test
    fun should_return_not_found_on_unknown_resource() {
        client.get().uri("/skus/b474894a")
                .exchange()
                .expectStatus()
                .isNotFound
    }

    @Throws(IOException::class)
    private fun createResource(resourceName: String): WebTestClient.ResponseSpec {
        return client.post().uri("/skus")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(Resources.toByteArray(Resources.getResource(resourceName)))
                .exchange()
                .expectStatus()
                .isCreated
    }

    @Throws(IOException::class)
    private fun createResources(resourceName: String): WebTestClient.ResponseSpec {
        return client.post().uri("/skus")
                .contentType(MediaType.parseMediaType("application/bulk+json"))
                .bodyValue(Resources.toByteArray(Resources.getResource(resourceName)))
                .exchange()
                .expectStatus()
                .isCreated
    }
}