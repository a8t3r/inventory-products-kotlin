package com.eordie.inventory.products.model.commons

import com.eordie.inventory.products.utils.Base64Serializer
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

class Page<T, C>(val items: Collection<T>, val paging: Paging<C>): Iterable<T> {

    override fun iterator(): Iterator<T> {
        return items.iterator()
    }

    class Paging<C> {
        @JsonSerialize(using = Base64Serializer::class)
        var nextCursor: C? = null
    }

    companion object {
        fun <C : Cursor<T>, T : Navigable> of(actualCursor: C, items: Flux<T>): Mono<Page<T, C>> {
            return items.collectList()
                    .map { list ->
                        var nextCursor: C? = null
                        if (list.isNotEmpty() && list.size == actualCursor.pageSize) {
                            val element = list[list.size - 1]
                            nextCursor = actualCursor
                            nextCursor.accept(element)
                        }

                        val paging = Paging<C>()
                        paging.nextCursor = nextCursor
                        return@map Page<T, C>(list, paging)
                    }
        }
    }
}