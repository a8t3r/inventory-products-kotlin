package com.eordie.inventory.products.model.event

import java.time.LocalDateTime
import java.util.*

enum class Type {
    SYSTEM,
    DATA
}

data class Event(val id: UUID = UUID.randomUUID(), val initializedAt: LocalDateTime = LocalDateTime.now(), val type: Type, val payload: Any? = null)