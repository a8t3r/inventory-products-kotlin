package com.eordie.inventory.products.model.product

import com.eordie.inventory.products.model.commons.Navigable
import com.eordie.inventory.products.utils.ResourceID
import com.eordie.inventory.products.utils.User
import com.fasterxml.jackson.annotation.JsonPropertyOrder
import java.math.BigDecimal
import java.time.LocalDateTime

@JsonPropertyOrder("id", "userId", "name", "brand", "price", "quantity", "tare", "tareCapacity", "createdAt", "updatedAt")
data class SkuView(
        val id: ResourceID,
        val userId: Int,
        val name: String,
        val brand: String,
        val price: BigDecimal,
        val quantity: BigDecimal,
        val tare: Tare,
        val tareCapacity: BigDecimal,
        val createdAt: LocalDateTime,
        val updatedAt: LocalDateTime
): Navigable {

    override fun cursor(): LocalDateTime {
        return createdAt
    }
}

fun Sku.toView(user: User) = SkuView(
        ResourceID(user.shardId, 1, id!!), user.userId,
        name, brand, price, quantity, tare, tareCapacity, createdAt, updatedAt
)
