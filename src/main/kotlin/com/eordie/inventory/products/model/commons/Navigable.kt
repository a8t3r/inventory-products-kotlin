package com.eordie.inventory.products.model.commons

import java.time.LocalDateTime

interface Navigable {

    fun cursor(): LocalDateTime

}