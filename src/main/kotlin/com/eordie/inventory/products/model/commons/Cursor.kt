package com.eordie.inventory.products.model.commons

interface Cursor<T> {

    val pageSize: Int

    fun accept(element: T)

}