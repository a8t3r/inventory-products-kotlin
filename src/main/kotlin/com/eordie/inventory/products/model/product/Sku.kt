package com.eordie.inventory.products.model.product

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import java.math.BigDecimal
import java.time.LocalDateTime
import javax.validation.constraints.NotNull
import javax.validation.constraints.PositiveOrZero
import javax.validation.constraints.Size

@Table("skus")
data class Sku(

        val name: String,

        val brand: String,

        val price: BigDecimal,

        val quantity: BigDecimal,

        val tare: Tare = Tare.milliliter,

        @Column("tare_capacity")
        val tareCapacity: BigDecimal,

        val userId: Int,

        @Id
        var id: Int? = null,

        @Column("created_at")
        val createdAt: LocalDateTime = LocalDateTime.now(),

        @Column("updated_at")
        var updatedAt: LocalDateTime = createdAt
) {
    constructor(proto: SkuProto, userId: Int) : this(proto.name!!, proto.brand!!, proto.price!!, proto.quantity!!, proto.tare!!, proto.tareCapacity!!, userId)

    fun mergeWith(proto: SkuProto): Sku = this.copy(
            name = proto.name ?: name,
            brand = proto.brand ?: brand,
            price = proto.price ?: price,
            quantity = proto.quantity ?: quantity,
            tare = proto.tare ?: tare,
            tareCapacity = proto.tareCapacity ?: tareCapacity
    )
}

class SkuProto {

    @Size(min = 1, max = 256)
    @NotNull(groups = [Create::class])
    var name: String? = null

    @Size(min = 1, max = 256)
    @NotNull(groups = [Create::class])
    var brand: String? = null

    @PositiveOrZero
    @NotNull(groups = [Create::class])
    var price: BigDecimal? = null

    @NotNull(groups = [Create::class])
    var quantity: BigDecimal? = null

    @NotNull(groups = [Create::class])
    var tare: Tare? = null

    @PositiveOrZero
    @NotNull(groups = [Create::class])
    var tareCapacity: BigDecimal? = null

    interface Create

}

enum class Tare {

    milliliter,
    liter,
    gram

}