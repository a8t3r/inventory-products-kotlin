package com.eordie.inventory.products.api

import com.eordie.inventory.products.model.commons.Cursor
import com.eordie.inventory.products.model.product.SkuView
import java.math.BigDecimal
import java.time.LocalDateTime
import java.time.ZoneOffset

class SkuCursor(
        val name: String?,
        val brand: String?,
        val quantityLte: BigDecimal?,
        val quantityGte: BigDecimal?,
        val priceLte: BigDecimal?,
        val priceGte: BigDecimal?,
        override val pageSize: Int,
        var identifier: Int = 0,
        var createdAt: LocalDateTime = LocalDateTime.ofEpochSecond(0, 0, ZoneOffset.UTC)) : Cursor<SkuView> {

    override fun accept(element: SkuView) {
        this.createdAt = element.createdAt
        this.identifier = element.id.localId
    }
}