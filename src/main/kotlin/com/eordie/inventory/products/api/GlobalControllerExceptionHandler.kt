package com.eordie.inventory.products.api

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.server.ServerWebExchange
import org.zalando.problem.Problem
import org.zalando.problem.Status
import org.zalando.problem.spring.webflux.advice.ProblemHandling
import org.zalando.problem.spring.webflux.advice.security.SecurityAdviceTrait
import reactor.core.publisher.Mono


@ControllerAdvice
class GlobalControllerExceptionHandler: ProblemHandling, SecurityAdviceTrait {

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @ExceptionHandler(ResourceAccessException::class)
    fun handleNotFound(): ResponseEntity<String> {
        return ResponseEntity.notFound().build()
    }

    @ExceptionHandler(ResourceConflictException::class)
    fun handleConflict(e: ResourceConflictException, exchange: ServerWebExchange): Mono<ResponseEntity<Problem>>? {
        return create(Problem.valueOf(Status.CONFLICT, e.message ?: "resource conflict"), exchange)
    }

     override fun formatFieldName(fieldName: String): String {
         return objectMapper.propertyNamingStrategy.nameForField(null, null, fieldName)
    }
}

class ResourceAccessException(message: String): RuntimeException(message)
class ResourceConflictException(message: String): RuntimeException(message)