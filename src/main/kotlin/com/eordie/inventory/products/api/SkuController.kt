package com.eordie.inventory.products.api

import com.eordie.inventory.products.config.internal.CustomMediaTypes.APPLICATION_BULK_JSON
import com.eordie.inventory.products.config.internal.CustomMediaTypes.APPLICATION_EXCEL_VALUE
import com.eordie.inventory.products.config.internal.CustomMediaTypes.APPLICATION_EXCEL_X_VALUE
import com.eordie.inventory.products.config.internal.CustomMediaTypes.TEXT_CSV_VALUE
import com.eordie.inventory.products.model.commons.Page
import com.eordie.inventory.products.model.event.Event
import com.eordie.inventory.products.model.product.Sku
import com.eordie.inventory.products.model.product.SkuProto
import com.eordie.inventory.products.model.product.SkuView
import com.eordie.inventory.products.model.product.toView
import com.eordie.inventory.products.service.SkuService
import com.eordie.inventory.products.utils.ResourceID
import com.eordie.inventory.products.utils.User
import kotlinx.coroutines.Dispatchers.Unconfined
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.reactor.mono
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType.APPLICATION_STREAM_JSON_VALUE
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.security.core.Authentication
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.math.BigDecimal
import java.time.Duration
import java.time.LocalDateTime
import java.time.ZoneOffset
import javax.validation.groups.Default

@Validated
@RestController
@RequestMapping("/skus")
@PreAuthorize("hasAnyAuthority('sku:read', 'sku:write')")
class SkuController {

    @Autowired
    private lateinit var service: SkuService

    @GetMapping
    @PreAuthorize("hasAuthority('sku:read')")
    fun find(
            user: User,
            @RequestParam(name = "name", required = false) name: String?,
            @RequestParam(name = "brand", required = false) brand: String?,
            @RequestParam(name = "quantity<", required = false) quantityLte: BigDecimal?,
            @RequestParam(name = "quantity>", required = false) quantityGte: BigDecimal?,
            @RequestParam(name = "price<", required = false) priceLte: BigDecimal?,
            @RequestParam(name = "price>", required = false) priceGte: BigDecimal?,
            @RequestParam(name = "page_size", required = false, defaultValue = "100") pageSize: Int,
            @RequestParam(name = "cursor", required = false) cursor: SkuCursor?): Mono<Page<SkuView, SkuCursor>> {

        val actualCursor = cursor?: SkuCursor(name, brand, quantityLte, quantityGte, priceLte, priceGte, pageSize)
        val source = service.findPage(user.userId, actualCursor).map { it.toView(user) }
        return Page.of(actualCursor, source)
    }

    @FlowPreview
    @PostMapping(
            consumes = [APPLICATION_BULK_JSON, TEXT_CSV_VALUE, APPLICATION_EXCEL_VALUE, APPLICATION_EXCEL_X_VALUE]
    )
    @ExperimentalCoroutinesApi
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("hasAuthority('sku:write')")
    fun create(user: User, @Validated(SkuProto.Create::class, Default::class) @RequestBody entities: Flux<SkuProto>, @RequestParam("limit", defaultValue = "2147483647") limit: Long): Flux<SkuView> {
        val input = entities.map { Sku(it, user.userId) }
                .take(limit)
                .delayElements(Duration.ofMillis(50))

        return service.insert(input).map { it.toView(user) }
    }

    @PostMapping
    @ExperimentalCoroutinesApi
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("hasAuthority('sku:write')")
    fun createOne(user: User, @Validated(SkuProto.Create::class, Default::class) @RequestBody prototype: SkuProto): Mono<SkuView> = mono(Unconfined) {
        val value = Sku(prototype, user.userId)
        service.insert(value).toView(user)
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    @GetMapping(produces = [ APPLICATION_STREAM_JSON_VALUE ])
    @PreAuthorize("hasAuthority('sku:read')")
    fun listen(authentication: Authentication, @RequestParam(name = "created_at", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) fromRef: LocalDateTime?): Flux<Event> {
        val from = fromRef ?: LocalDateTime.ofEpochSecond(0, 0, ZoneOffset.UTC)
        return service.listen(authentication, from)
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('sku:read')")
    fun findOne(user: User, @PathVariable("id") id: ResourceID): Mono<SkuView> = mono(Unconfined) {
        (service.findOne(user.userId, id.localId) ?: throw ResourceAccessException("not found")).toView(user)
    }

    @PatchMapping("/{id}")
    @PreAuthorize("hasAuthority('sku:write')")
    fun updateOne(user: User, @PathVariable("id") id: ResourceID, @Validated @RequestBody prototype: SkuProto): Mono<SkuView> = mono(Unconfined) {
        val value = (service.findOne(user.userId, id.localId) ?: throw ResourceAccessException("not found"))
                .mergeWith(prototype)

        service.updateOne(value).toView(user)
    }

    @FlowPreview
    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('sku:write')")
    fun replaceOne(user: User, @PathVariable("id") id: ResourceID, @Validated(SkuProto.Create::class, Default::class) @RequestBody prototype: SkuProto): Mono<SkuView> = mono(Unconfined) {
        val value = (service.findOne(user.userId, id.localId) ?: throw ResourceConflictException("id initialization is not allowed"))
                .mergeWith(prototype)

        service.updateOne(value).toView(user)
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority('sku:write')")
    fun delete(user: User, @PathVariable("id") id: ResourceID) = mono(Unconfined) {
        val value = service.findOne(user.userId, id.localId) ?: throw ResourceAccessException("not found")
        service.delete(value.id!!)
    }
}
