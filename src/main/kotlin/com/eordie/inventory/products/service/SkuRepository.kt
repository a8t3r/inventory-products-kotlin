package com.eordie.inventory.products.service

import com.eordie.inventory.products.model.product.Sku
import org.springframework.data.r2dbc.repository.R2dbcRepository
import org.springframework.data.r2dbc.repository.query.Query
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.math.BigDecimal
import java.time.LocalDateTime

@Repository
interface SkuRepository: R2dbcRepository<Sku, Int> {

    @Query("select * from skus " +
            "where id = :id and user_id = :userId")
    fun findById(userId: Int, id: Int): Mono<Sku>

    @Query("select * from skus " +
            "where (name = :name or :name is null) and " +
            "(brand = :brand or :brand is null) and " +
            "(quantity <= :quantityLte or :quantityLte is null) and " +
            "(quantity >= :quantityGte or :quantityGte is null) and " +
            "(price <= :priceLte or :priceLte is null) and " +
            "(price >= :priceGte or :priceGte is null) and " +
            "(created_at > :createdAt or (created_at = :createdAt and id > :identifier)) and user_id = :userId " +
            "order by created_at, id limit :limit")
    fun findByCursor(userId: Int, name: String?, brand: String?,
                     quantityLte: BigDecimal?, quantityGte: BigDecimal?,
                     priceLte: BigDecimal?, priceGte: BigDecimal?,
                     createdAt: LocalDateTime, identifier: Int, limit: Int): Flux<Sku>

    @Query("select * from skus where updated_at > :updatedAt and user_id = :userId")
    fun findByUpdatedAt(userId: Int, updatedAt: LocalDateTime): Flux<Sku>

}