package com.eordie.inventory.products.service

import com.eordie.inventory.products.api.SkuCursor
import com.eordie.inventory.products.model.event.Event
import com.eordie.inventory.products.model.event.Type
import com.eordie.inventory.products.model.product.Sku
import com.eordie.inventory.products.model.product.toView
import com.eordie.inventory.products.utils.User
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.reactive.awaitFirst
import kotlinx.coroutines.reactive.awaitFirstOrNull
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.ReactiveSecurityContextHolder
import org.springframework.security.core.context.SecurityContextImpl
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.FluxSink
import reactor.core.publisher.Mono
import reactor.core.scheduler.Schedulers
import java.time.Duration
import java.time.LocalDateTime
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.atomic.AtomicReference
import java.util.concurrent.locks.LockSupport


@Service
class SkuService {

    @Autowired
    private lateinit var repository: SkuRepository

    fun findPage(userId: Int, cursor: SkuCursor): Flux<Sku> {
        return repository.findByCursor(userId, cursor.name, cursor.brand,
                cursor.quantityLte, cursor.quantityGte,
                cursor.priceLte, cursor.priceGte,
                cursor.createdAt, cursor.identifier, cursor.pageSize)
    }

    suspend fun delete(id: Int) {
        repository.deleteById(id).awaitFirstOrNull()
    }

    suspend fun findOne(userId: Int, id: Int): Sku? {
        return repository.findById(userId, id).awaitFirstOrNull()
    }

    suspend fun updateOne(sku: Sku): Sku {
        sku.updatedAt = LocalDateTime.now()
        return repository.save(sku).awaitFirst()
    }

    private fun internal(authentication: Authentication, from: LocalDateTime): Flux<Event> {
        val securityContext = SecurityContextImpl()
        securityContext.authentication = authentication
        val user = authentication.principal as User

        return Flux.create { sink: FluxSink<Event> ->
            val fromRef = AtomicReference(from)
            val isCompleted = AtomicBoolean(false)
            while (!Thread.currentThread().isInterrupted) {
                if (isCompleted.compareAndSet(false, true)) {
                    repository.findByUpdatedAt(user.userId, fromRef.get())
                            .doOnNext { sink.next(Event(payload = it.toView(user), type = Type.DATA)) }
                            .doOnNext { fromRef.set(it.updatedAt) }
                            .doOnComplete { isCompleted.set(false) }
                            .subscriberContext(ReactiveSecurityContextHolder.withSecurityContext(Mono.just(securityContext)))
                            .subscribe()
                } else {
                    LockSupport.parkNanos(TimeUnit.SECONDS.toNanos(5))
                }
            }
        }
    }

    @FlowPreview
    fun listen(authentication: Authentication, from: LocalDateTime): Flux<Event> {
        val initial = internal(authentication, from)
                .subscribeOn(Schedulers.elastic(), false)

        val systemPing = Flux.interval(Duration.ofSeconds(30))
                .map { Event(type = Type.SYSTEM) }

        return Flux.just(Event(type = Type.SYSTEM))
                .mergeWith(initial)
                .mergeWith(systemPing)
    }

    suspend fun insert(input: Sku): Sku {
        return repository.save(input).awaitFirst()
    }

    @FlowPreview
    fun insert(input: Flux<Sku>): Flux<Sku> {
        return input.flatMap { repository.save(it) }
    }
}
