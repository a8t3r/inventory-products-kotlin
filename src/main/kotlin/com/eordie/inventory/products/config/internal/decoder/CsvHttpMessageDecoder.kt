package com.eordie.inventory.products.config.internal.decoder

import com.coreoz.windmill.imports.Parsers
import com.coreoz.windmill.imports.parsers.csv.CsvParserConfig
import com.eordie.inventory.products.config.internal.CustomMediaTypes
import com.fasterxml.jackson.databind.ObjectMapper

import java.nio.charset.Charset
import java.util.*

class CsvHttpMessageDecoder<T>(mapper: ObjectMapper, bundle: ResourceBundle) : AbstractRowDecoder<T>(bundle, Parsers.csv(CsvParserConfig.builder()
        .charset(Charset.defaultCharset())
        .separator(';')
        .build()), mapper, CustomMediaTypes.TEXT_CSV)
