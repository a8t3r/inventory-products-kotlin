package com.eordie.inventory.products.config.internal.decoder

import com.coreoz.windmill.files.FileSource
import com.coreoz.windmill.imports.FileParser
import com.coreoz.windmill.imports.Row
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import org.reactivestreams.Publisher
import org.springframework.core.ResolvableType
import org.springframework.core.codec.AbstractDecoder
import org.springframework.core.io.buffer.DataBuffer
import org.springframework.core.io.buffer.DataBufferUtils
import org.springframework.util.MimeType
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.util.*

abstract class AbstractRowDecoder<T>(private val bundle: ResourceBundle, private val parser: FileParser, private val mapper: ObjectMapper, supportedMimeType: MimeType) : AbstractDecoder<T>(supportedMimeType) {

    override fun decodeToMono(inputStream: Publisher<DataBuffer>, elementType: ResolvableType, mimeType: MimeType?, hints: MutableMap<String, Any>?): Mono<T> {
        return decode(inputStream, elementType, mimeType, hints)
                .take(1).single()
    }

    override fun decode(inputStream: Publisher<DataBuffer>, elementType: ResolvableType, mimeType: MimeType?, hints: Map<String, Any>?): Flux<T> {
        return Flux.from(DataBufferUtils.join(inputStream))
                .flatMap<Row> { Flux.fromStream(parser.parse(FileSource.of(it.asInputStream()))) }
                .skip(1)
                .map { instantiate(elementType, it) }
    }

    /**
     * Naive Row to Object transformation
     */
    @Throws(ReflectiveOperationException::class)
    private fun instantiate(elementType: ResolvableType, row: Row): T {
        val instance = elementType.rawClass!!.newInstance()
        val intermediate = mapper.convertValue(instance, object : TypeReference<Map<String, Any>>() {}).toMutableMap()
        for (fieldName in intermediate.keys) {
            val alias = bundle.getString("${elementType.rawClass!!.name}.$fieldName") ?: fieldName
            if (row.columnExists(alias)) {
                intermediate[fieldName] = row.cell(alias).asString()
            }
        }

        return mapper.convertValue(intermediate, elementType.rawClass) as T
    }
}
