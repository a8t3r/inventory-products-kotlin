package com.eordie.inventory.products.config.internal.decoder

import com.coreoz.windmill.imports.Parsers
import com.eordie.inventory.products.config.internal.CustomMediaTypes
import com.fasterxml.jackson.databind.ObjectMapper
import java.util.*

class XlsxHttpMessageDecoder<T>(mapper: ObjectMapper, bundle: ResourceBundle) : AbstractRowDecoder<T>(bundle, Parsers.xlsx(), mapper, CustomMediaTypes.APPLICATION_EXCEL_X)
