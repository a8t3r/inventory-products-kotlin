package com.eordie.inventory.products.config.internal.encoder

import com.coreoz.windmill.exports.config.ExportColumn
import com.coreoz.windmill.exports.config.ExportHeaderMapping
import com.coreoz.windmill.exports.exporters.Exporter
import com.fasterxml.jackson.annotation.JsonPropertyOrder
import org.springframework.beans.BeanUtils
import org.springframework.core.ResolvableType
import org.springframework.core.codec.AbstractEncoder
import org.springframework.core.io.buffer.DataBuffer
import org.springframework.util.MimeType
import java.beans.PropertyDescriptor
import java.util.*
import kotlin.streams.toList

abstract class AbstractRowEncoder<T>(private val bundle: ResourceBundle, supportedMimeTypes: MimeType) : AbstractEncoder<T>(supportedMimeTypes) {

    fun getExportHeaders(elementType: ResolvableType): ExportHeaderMapping<T> {
        val targetType = if (elementType.hasGenerics()) elementType.getGeneric(0) else elementType
        val rawClass = targetType.rawClass!!
        val ordered = linkedSetOf(*(rawClass.getDeclaredAnnotation(JsonPropertyOrder::class.java)?.value ?: emptyArray())).reversed()

        val columns = Arrays.stream<PropertyDescriptor>(BeanUtils.getPropertyDescriptors(targetType.rawClass!!))
                .filter { descriptor -> descriptor.propertyType != Class::class.java }
                .sorted { a, b ->
                    val byIndex = ordered.indexOf(a.name).compareTo(ordered.indexOf(b.name))
                    if (byIndex == 0) a.name.compareTo(b.name) else -byIndex
                }
                .map { descriptor ->
                    val alias = bundle.getString("${rawClass.name}.${descriptor.name}") ?: descriptor.name
                    ExportColumn.of(alias) { it: T ->
                        try {
                            return@of descriptor.readMethod.invoke(it)
                        } catch (e: IllegalArgumentException) {
                            throw IllegalStateException(e)
                        } catch (e: ReflectiveOperationException) {
                            throw IllegalStateException(e)
                        }
                    }
                }
                .toList()

        return ExportHeaderMapping(columns)
    }

    open fun buildExporter(elementType: ResolvableType, dataBuffer: DataBuffer): Exporter<T> {
        throw java.lang.UnsupportedOperationException()
    }
}
