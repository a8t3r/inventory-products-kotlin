package com.eordie.inventory.products.config.internal.encoder

import com.coreoz.windmill.exports.config.ExportConfig
import com.coreoz.windmill.exports.exporters.Exporter
import com.coreoz.windmill.exports.exporters.csv.ExportCsvConfig
import com.eordie.inventory.products.config.internal.CustomMediaTypes
import org.reactivestreams.Publisher
import org.springframework.core.ResolvableType
import org.springframework.core.codec.Hints
import org.springframework.core.io.buffer.DataBuffer
import org.springframework.core.io.buffer.DataBufferFactory
import org.springframework.core.log.LogFormatUtils
import org.springframework.util.MimeType
import reactor.core.publisher.Flux
import java.io.ByteArrayOutputStream
import java.nio.charset.Charset
import java.util.*

class CsvHttpMessageEncoder<T>(bundle: ResourceBundle) : AbstractRowEncoder<T>(bundle, CustomMediaTypes.TEXT_CSV) {

    private val config: ExportCsvConfig = ExportCsvConfig.builder()
            .charset(Charset.defaultCharset())
            .separator(';')
            .build()

    override fun encode(inputStream: Publisher<out T>, bufferFactory: DataBufferFactory, elementType: ResolvableType, mimeType: MimeType?, hints: Map<String, Any>?): Flux<DataBuffer> {
        val buffer = ByteArrayOutputStream()
        val exporter = ExportConfig<T>(buffer)
                .withHeaderMapping(getExportHeaders(elementType))
                .asCsv(config)

        return Flux.from(inputStream)
                .map {
                    val dataBuffer = encodeValue(buffer, exporter, it, bufferFactory, hints)
                    buffer.reset()
                    dataBuffer
                }
                .doOnComplete { exporter.close() }
    }

    override fun encodeValue(value: T, bufferFactory: DataBufferFactory, valueType: ResolvableType, mimeType: MimeType?, hints: MutableMap<String, Any>?): DataBuffer {
        val buffer = ByteArrayOutputStream()
        val exporter = ExportConfig<T>(buffer)
                .withHeaderMapping(getExportHeaders(valueType))
                .asCsv(config)

        if (value is Iterable<*>) {
            val source = (value as Iterable<T>).toList()
            return bufferFactory.join(source.map {
                val dataBuffer = encodeValue(buffer, exporter, it, bufferFactory, hints)
                buffer.reset()
                dataBuffer
            })
        }

        val encodeValue = encodeValue(buffer, exporter, value, bufferFactory, hints)
        exporter.close()
        return encodeValue
    }

    private fun encodeValue(buffer: ByteArrayOutputStream, exporter: Exporter<T>, value: T, bufferFactory: DataBufferFactory, hints: Map<String, Any>?): DataBuffer {
        if (!Hints.isLoggingSuppressed(hints)) {
            LogFormatUtils.traceDebug(logger) { traceOn ->
                val formatted = LogFormatUtils.formatValue(value, !traceOn)
                Hints.getLogPrefix(hints) + "Encoding [" + formatted + "]"
            }
        }

        val dataBuffer = bufferFactory.allocateBuffer()
        exporter.writeRow(value)
        dataBuffer.write(buffer.toByteArray())
        return dataBuffer
    }
}
