package com.eordie.inventory.products.config

import com.eordie.inventory.products.utils.User
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.security.authentication.ReactiveAuthenticationManager
import org.springframework.security.config.annotation.method.configuration.EnableReactiveMethodSecurity
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity
import org.springframework.security.config.web.server.ServerHttpSecurity
import org.springframework.security.core.context.ReactiveSecurityContextHolder
import org.springframework.security.web.server.SecurityWebFilterChain
import reactor.core.publisher.Mono


@EnableWebFluxSecurity
@EnableReactiveMethodSecurity
class SecurityConfig {

    @Autowired
    private lateinit var authenticationManager: ReactiveAuthenticationManager

    @Autowired
    private lateinit var securityContextRepository: SecurityContextRepository

    @Bean
    fun securityWebFilterChain(http: ServerHttpSecurity): SecurityWebFilterChain {
        return http.csrf().disable()
                .authorizeExchange()
                .pathMatchers("/v2/api-docs", "/actuator/**").permitAll()
                .and()
                .httpBasic().disable()
                .authenticationManager(authenticationManager)
                .securityContextRepository(securityContextRepository)
                .authorizeExchange()
                .anyExchange().authenticated()
                .and().build()
    }

    @Bean
    fun authenticationManager(): ReactiveAuthenticationManager {
        return JwtTokenAuthenticationManager()
    }

    @Bean
    fun securityContextRepository(): SecurityContextRepository {
        return SecurityContextRepository()
    }

    @Bean
    fun shardFunction(): Mono<Any> {
        return ReactiveSecurityContextHolder.getContext()
                .switchIfEmpty(Mono.error(IllegalStateException("ReactiveSecurityContext is empty")))
                .map { it.authentication.principal }
                .cast(User::class.java)
                .map { it.shardId }
    }
}