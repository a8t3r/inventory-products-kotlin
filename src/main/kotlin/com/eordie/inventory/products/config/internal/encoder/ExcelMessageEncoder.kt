package com.eordie.inventory.products.config.internal.encoder

import org.reactivestreams.Publisher
import org.springframework.core.ResolvableType
import org.springframework.core.io.buffer.DataBuffer
import org.springframework.core.io.buffer.DataBufferFactory
import org.springframework.util.MimeType
import reactor.core.publisher.Flux
import java.util.*

abstract class ExcelMessageEncoder<T>(bundle: ResourceBundle, supportedMimeTypes: MimeType) : AbstractRowEncoder<T>(bundle, supportedMimeTypes) {

    override fun encodeValue(value: T, bufferFactory: DataBufferFactory, valueType: ResolvableType, mimeType: MimeType?, hints: MutableMap<String, Any>?): DataBuffer {
        if (value is Iterable<*>) {
            val items = (value as Iterable<T>).toList()
            val dataBuffer = bufferFactory.allocateBuffer()
            val exporter = buildExporter(valueType, dataBuffer)

            items.forEach { exporter.writeRow(it) }
            exporter.close()

            return dataBuffer
        }

        throw UnsupportedOperationException("Only page type supported")
    }

    override fun encode(inputStream: Publisher<out T>, bufferFactory: DataBufferFactory, elementType: ResolvableType, mimeType: MimeType?, hints: Map<String, Any>?): Flux<DataBuffer> {
        val dataBuffer = bufferFactory.allocateBuffer()
        val exporter = buildExporter(elementType, dataBuffer)

        return Flux.from(inputStream)
                .doOnNext { exporter.writeRow(it) }
                .doOnComplete { exporter.close() }
                .thenMany(Flux.just(dataBuffer))
    }
}