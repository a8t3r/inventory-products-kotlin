package com.eordie.inventory.products.config.internal.encoder

import com.coreoz.windmill.exports.config.ExportConfig
import com.coreoz.windmill.exports.exporters.Exporter
import com.coreoz.windmill.exports.exporters.excel.ExportExcelConfig
import com.eordie.inventory.products.config.internal.CustomMediaTypes
import org.springframework.core.ResolvableType
import org.springframework.core.io.buffer.DataBuffer
import java.util.*

class XlsxHttpMessageEncoder<T>(bundle: ResourceBundle) : ExcelMessageEncoder<T>(bundle, CustomMediaTypes.APPLICATION_EXCEL_X) {

    override fun buildExporter(elementType: ResolvableType, dataBuffer: DataBuffer): Exporter<T> {
        return ExportConfig<T>(dataBuffer.asOutputStream())
                .withHeaderMapping(getExportHeaders(elementType))
                .asExcel(ExportExcelConfig.newXlsxFile().build())
    }
}
