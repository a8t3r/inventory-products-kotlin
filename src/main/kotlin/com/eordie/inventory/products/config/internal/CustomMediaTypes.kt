package com.eordie.inventory.products.config.internal

import org.springframework.http.MediaType

object CustomMediaTypes {

    const val TEXT_CSV_VALUE = "text/csv;q=0.1"
    const val APPLICATION_EXCEL_VALUE = "application/vnd.ms-excel;q=0.1"
    const val APPLICATION_EXCEL_X_VALUE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;q=0.1"
    const val APPLICATION_BULK_JSON = "application/bulk+json"

    val TEXT_CSV = MediaType.parseMediaType(TEXT_CSV_VALUE)
    val APPLICATION_EXCEL = MediaType.parseMediaType(APPLICATION_EXCEL_VALUE)
    val APPLICATION_EXCEL_X = MediaType.parseMediaType(APPLICATION_EXCEL_X_VALUE)

}
