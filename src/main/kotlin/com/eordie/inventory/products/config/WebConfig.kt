package com.eordie.inventory.products.config

import com.eordie.inventory.products.config.internal.decoder.CsvHttpMessageDecoder
import com.eordie.inventory.products.config.internal.decoder.XlsHttpMessageDecoder
import com.eordie.inventory.products.config.internal.decoder.XlsxHttpMessageDecoder
import com.eordie.inventory.products.config.internal.encoder.CsvHttpMessageEncoder
import com.eordie.inventory.products.config.internal.encoder.XlsHttpMessageEncoder
import com.eordie.inventory.products.config.internal.encoder.XlsxHttpMessageEncoder
import com.eordie.inventory.products.utils.Base64ParamConverterFactory
import com.eordie.inventory.products.utils.ResourceID
import com.eordie.inventory.products.utils.User
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.PropertyNamingStrategy
import com.fasterxml.jackson.databind.SerializationFeature
import org.springframework.boot.web.codec.CodecCustomizer
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.MethodParameter
import org.springframework.core.convert.converter.Converter
import org.springframework.format.FormatterRegistry
import org.springframework.security.core.Authentication
import org.springframework.web.reactive.BindingContext
import org.springframework.web.reactive.config.WebFluxConfigurer
import org.springframework.web.reactive.result.method.HandlerMethodArgumentResolver
import org.springframework.web.reactive.result.method.annotation.ArgumentResolverConfigurer
import org.springframework.web.server.ServerWebExchange
import reactor.core.publisher.Mono
import java.security.Principal
import java.util.*


@Configuration
class WebConfig: WebFluxConfigurer {

    companion object {
        val defaultMapper = ObjectMapper()
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
                .setDefaultPropertyInclusion(JsonInclude.Include.NON_NULL)
                .setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE)
                .findAndRegisterModules()!!
    }

    @Bean
    fun objectMapper(): ObjectMapper {
        return defaultMapper
    }

    @Bean
    fun codecCustomizer(): CodecCustomizer {
        val mapper = ObjectMapper()
        val bundle = ResourceBundle.getBundle("localization")

        return CodecCustomizer { configurer ->
            val codecs = configurer.customCodecs()
            codecs.decoder(XlsHttpMessageDecoder<Any>(mapper, bundle))
            codecs.decoder(XlsxHttpMessageDecoder<Any>(mapper, bundle))
            codecs.decoder(CsvHttpMessageDecoder<Any>(mapper, bundle))

            codecs.encoder(CsvHttpMessageEncoder<Any>(bundle))
            codecs.encoder(XlsHttpMessageEncoder<Any>(bundle))
            codecs.encoder(XlsxHttpMessageEncoder<Any>(bundle))
        }
    }

    override fun addFormatters(registry: FormatterRegistry) {
        registry.addConverterFactory(Base64ParamConverterFactory())
        registry.addConverter(StringToResourceIDConverter())
        registry.addConverter(ResourceIDToStringConverter())
    }

    override fun configureArgumentResolvers(configurer: ArgumentResolverConfigurer) {
        configurer.addCustomResolver(UserMethodArgumentResolver())
    }
}

class UserMethodArgumentResolver: HandlerMethodArgumentResolver {

    override fun supportsParameter(parameter: MethodParameter): Boolean {
        return User::class.java.isAssignableFrom(parameter.parameterType)
    }

    override fun resolveArgument(parameter: MethodParameter, context: BindingContext, exchange: ServerWebExchange): Mono<Any> {
        return exchange.getPrincipal<Principal>()
                .ofType(Authentication::class.java)
                .map { it.principal }
    }
}

class StringToResourceIDConverter : Converter<String, ResourceID> {
    override fun convert(id: String): ResourceID {
        return ResourceID.parse(id)
    }
}


class ResourceIDToStringConverter : Converter<ResourceID, String> {
    override fun convert(id: ResourceID): String {
        return id.toString()
    }
}