package com.eordie.inventory.products.config

import io.r2dbc.postgresql.PostgresqlConnectionConfiguration
import io.r2dbc.postgresql.PostgresqlConnectionFactory
import io.r2dbc.spi.ConnectionFactory
import io.r2dbc.spi.ConnectionFactoryMetadata
import org.flywaydb.core.Flyway
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.r2dbc.config.AbstractR2dbcConfiguration
import org.springframework.data.r2dbc.connectionfactory.lookup.AbstractRoutingConnectionFactory
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories
import reactor.core.publisher.Mono
import javax.annotation.PostConstruct

@Configuration
@EnableConfigurationProperties(MultiTenantSourceProperties::class)
@ConditionalOnProperty(value = ["spring.multitenant.enabled"], havingValue = "true", matchIfMissing = true)
@EnableR2dbcRepositories(basePackages = ["com.eordie.inventory.products.service"])
class MultiTenantDatabaseConfiguration : AbstractR2dbcConfiguration() {

    @Autowired
    private lateinit var properties: MultiTenantSourceProperties

    @Autowired
    private lateinit var shardFunction: Mono<Any>

    @Bean
    override fun connectionFactory(): ConnectionFactory {
        val factories = properties.sources.entries.asSequence()
                .map { Pair(it.key, PostgresqlConnectionFactory(it.value.asConfiguration())) }
                .toMap()

        return MultiTenantConnectionFactory(factories, shardFunction)
    }

    @PostConstruct
    fun flyway() {
        properties.sources.entries.asSequence()
                .forEach { val value = it.value
                    val flyway = Flyway.configure().dataSource(value.url(), value.username, value.password).load()
                    flyway.migrate()
                }
    }
}

class MultiTenantConnectionFactory(targetConnectionFactories: Map<Short, PostgresqlConnectionFactory>, private val shardFunction: Mono<Any>) : AbstractRoutingConnectionFactory() {

    private val defaultMeta: ConnectionFactoryMetadata
    private val instances: Int

    init {
        super.setTargetConnectionFactories(targetConnectionFactories)
        defaultMeta = targetConnectionFactories.values.first().metadata
        instances = targetConnectionFactories.size
    }

    override fun getMetadata(): ConnectionFactoryMetadata {
        return defaultMeta
    }

    override fun determineCurrentLookupKey(): Mono<Any> {
        return shardFunction
    }
}

@ConstructorBinding
@ConfigurationProperties(prefix = "spring.multitenant")
data class MultiTenantSourceProperties(val sources: Map<Short, SourceProperties>, val enabled: Boolean = true)

@ConstructorBinding
data class SourceProperties(val database: String, val host: String, val port: Int = 5432, val username: String, val password: String) {

    fun asConfiguration(): PostgresqlConnectionConfiguration {
        val builder = PostgresqlConnectionConfiguration.builder()
                .database(database)
                .host(host)
                .port(port)
                .password(password)
                .username(username)

        return builder.build()
    }

    fun url(): String {
        return "jdbc:postgresql://$host:$port/$database"
    }
}