package com.eordie.inventory.products.config

import com.eordie.inventory.products.utils.User
import com.eordie.inventory.products.utils.logster
import io.jsonwebtoken.Claims
import io.jsonwebtoken.JwtException
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import org.springframework.security.authentication.ReactiveAuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import reactor.core.publisher.Mono
import java.util.*


class JwtTokenAuthenticationManager: ReactiveAuthenticationManager {

    override fun authenticate(authentication: Authentication?): Mono<Authentication> {
        val jwtToken = authentication!!.credentials.toString()

        val user = TokenFactory.parseFromToken(jwtToken)
        return if (user != null) {
            Mono.just(UsernamePasswordAuthenticationToken(user, null, user.authorities))
        } else {
            Mono.empty()
        }
    }
}

object TokenFactory {

    private const val signatureKey = ":W,[/}4G<94)GV#PfRt8guHr/}(9,dgD"
    private val logger by logster

    private fun claimsFromToken(token: String): Claims? {
        return try {
            Jwts.parser()
                    .setSigningKey(signatureKey.toByteArray())
                    .parseClaimsJws(token).body
        } catch (e: JwtException) {
            logger.warn("invalid token: {}", e.message)
            null
        }
    }

    fun parseFromToken(token: String): User? {
        val claims = claimsFromToken(token) ?: return null

        return User(
                claims["user_id"] as Int,
                (claims["shard_id"] as Int).toShort(),
                claims.subject,
                "*".repeat(10),
                claims["roles", (List::class.java as Class<List<String>>)]
        )
    }

    fun createToken(user: User): String {
        return Jwts.builder()
                .signWith(SignatureAlgorithm.HS256, Base64.getEncoder().encodeToString(signatureKey.toByteArray()))
                .setSubject(user.username)
                .claim("user_id", user.userId)
                .claim("shard_id", user.shardId)
                .claim("roles", user.roles)
                .compact()
    }
}