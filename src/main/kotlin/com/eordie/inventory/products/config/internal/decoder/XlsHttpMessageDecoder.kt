package com.eordie.inventory.products.config.internal.decoder

import com.coreoz.windmill.imports.Parsers
import com.eordie.inventory.products.config.internal.CustomMediaTypes
import com.fasterxml.jackson.databind.ObjectMapper
import java.util.*

class XlsHttpMessageDecoder<T>(mapper: ObjectMapper, bundle: ResourceBundle) : AbstractRowDecoder<T>(bundle, Parsers.xls(), mapper, CustomMediaTypes.APPLICATION_EXCEL)
