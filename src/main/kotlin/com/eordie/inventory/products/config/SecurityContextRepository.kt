package com.eordie.inventory.products.config

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.security.authentication.ReactiveAuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContext
import org.springframework.security.core.context.SecurityContextImpl
import org.springframework.security.web.server.context.ServerSecurityContextRepository
import org.springframework.web.server.ServerWebExchange
import reactor.core.publisher.Mono


class SecurityContextRepository: ServerSecurityContextRepository {

    @Autowired
    private lateinit var authenticationManager: ReactiveAuthenticationManager

    override fun load(exchange: ServerWebExchange?): Mono<SecurityContext> {
        val header = exchange!!.request.headers.getFirst(HttpHeaders.AUTHORIZATION)
        return if (header != null && header.startsWith("Bearer ")) {
            val authToken = header.substring(7)
            val authenticationToken = UsernamePasswordAuthenticationToken(authToken, authToken)
            authenticationManager.authenticate(authenticationToken)
                    .map { val context = SecurityContextImpl()
                        context.authentication = it
                        context
                    }
        } else {
            Mono.empty()
        }
    }

    override fun save(exchange: ServerWebExchange?, context: SecurityContext?): Mono<Void> {
        throw UnsupportedOperationException()
    }
}