package com.eordie.inventory.products.utils

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonValue
import java.math.BigInteger
import java.util.Objects.requireNonNull
import java.util.regex.Pattern

/**
 * https://medium.com/pinterest-engineering/sharding-pinterest-how-we-scaled-our-mysql-fleet-3f341e96ca6f
 */
data class ResourceID(val shardId: Short, val typeId: Byte, val localId: Int) {

    @JsonValue
    override fun toString(): String {
        return Base62.encode(toNumber().toBigInteger())
    }

    fun toNumber(): Long {
        return shardId.toLong().shl(46)
                .or(typeId.toLong().shl(36)
                .or(localId.toLong().shl(0)))
    }

    companion object {

        @JvmStatic
        @JsonCreator
        fun parse(value: String): ResourceID {
            return parse(Base62.decode(value).toLong())
        }

        fun parse(value: Long): ResourceID {
            val shardId = value.shr(46).and(0xFFFF).toShort()
            val typeId = value.shr(36).and(0x3FF).toByte()
            val localId = value.shr(0).and(0xFFFFFFFFF).toInt()

            return ResourceID(shardId, typeId, localId)
        }
    }
}

/**
 * Base62 encoder/decoder.
 *
 * Source: https://github.com/opencoinage/opencoinage/blob/master/src/java/org/opencoinage/util/Base62.java
 */
internal object Base62 {

    private val BASE = BigInteger.valueOf(62)
    private const val DIGITS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"

    fun encode(number: BigInteger): String {
        if (number < BigInteger.ZERO) {
            throwIllegalArgumentException("number must not be negative")
        }

        var value  = number
        val result = StringBuilder()
        while (value > BigInteger.ZERO) {
            val divmod = value.divideAndRemainder(BASE)
            value = divmod[0]
            val digit = divmod[1].toInt()
            result.insert(0, DIGITS[digit])
        }
        return if (result.isEmpty()) DIGITS.substring(0, 1) else result.toString()
    }

    private fun throwIllegalArgumentException(format: String, vararg args: Any): BigInteger {
        throw IllegalArgumentException(String.format(format, *args))
    }

    @JvmOverloads
    fun decode(string: String, bitLimit: Int = 128): BigInteger {
        requireNonNull(string, "Decoded string must not be null")
        if (string.isEmpty()) {
            return throwIllegalArgumentException("String '%s' must not be empty", string)
        }

        if (!Pattern.matches("[$DIGITS]*", string)) {
            throwIllegalArgumentException("String '%s' contains illegal characters, only '%s' are allowed", string, DIGITS)
        }
        var result = BigInteger.ZERO
        val digits = string.length
        for (index in 0 until digits) {
            val digit = DIGITS.indexOf(string[digits - index - 1])
            result = result.add(BigInteger.valueOf(digit.toLong()).multiply(BASE.pow(index)))
            if (bitLimit > 0 && result.bitLength() > bitLimit) {
                throwIllegalArgumentException("String contains '%s' more than 128bit information (%sbit)", string, result.bitLength())
            }
        }

        return result
    }
}