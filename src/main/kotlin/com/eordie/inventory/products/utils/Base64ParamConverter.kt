package com.eordie.inventory.products.utils

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.JsonSerializer
import com.fasterxml.jackson.databind.SerializerProvider
import org.springframework.core.convert.converter.Converter
import org.springframework.core.convert.converter.ConverterFactory
import java.io.IOException
import java.util.*
import com.eordie.inventory.products.config.WebConfig.Companion.defaultMapper as mapper

class Base64Serializer : JsonSerializer<Any>() {

    companion object {
        fun serialize(value: Any): String {
            val encoder = Base64.getEncoder()

            val bytes = mapper.writeValueAsBytes(value)
            return encoder.encodeToString(bytes)
        }
    }

    @Throws(IOException::class)
    override fun serialize(value: Any, gen: JsonGenerator, serializers: SerializerProvider) {
        gen.writeString(Companion.serialize(value))
    }
}

class Base64ParamConverterFactory: ConverterFactory<String, Any> {

    override fun <T : Any?> getConverter(targetType: Class<T>): Converter<String, T> {
        return Base64ParamConverter(targetType)
    }

    class Base64ParamConverter<T>(private val targetClass: Class<T>): Converter<String, T> {
        override fun convert(source: String): T? {
            // todo: make clear difference with generic string type
            if (targetClass == String::class.java) {
                return source as T?
            }

            val decodedValue = Base64.getDecoder().decode(source)
            return mapper.readValue(decodedValue, targetClass)
        }
    }
}