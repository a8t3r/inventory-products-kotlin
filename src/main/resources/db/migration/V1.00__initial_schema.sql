create table skus (
  user_id int not null,
  id serial primary key,
  name varchar(256) not null,
  brand varchar(256),
  tare varchar(12) not null,
  tare_capacity numeric not null,
  price numeric,
  quantity numeric,
  created_at timestamp with time zone not null,
  updated_at timestamp with time zone not null
);